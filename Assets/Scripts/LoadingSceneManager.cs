﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingSceneManager : MonoBehaviour {
    [Header("Binding")]
    public static string loadingSceneName = "loading";

    [Header("Game objects")]
    public RawImage fader;
    public Text nowLoadingText;

    [Header("Others")]
    public float fadeDuration = 0.2f;
    public float loadCompleteDelay = 0.5f;
    public Color fadeColor = new Color(0f, 0f, 0f, 1f);
    public Color transparentColor = new Color(0f, 0f, 0f, 0f);
    public string startingLoadingText = "Now Loading";
    public string loopingEndLoadingText = "Now Loading...";

    private AsyncOperation _asyncOperation;
    private static string destinationSceneName = "";
    private bool _isFadingIn = true;
    private float _currentFadeTime = 0f;

    // Use this for initialization
    void Start () {
        if (destinationSceneName != "") {
            StartCoroutine(LoadAsync());
        }
        StartCoroutine(AnimateText());
	}
	
	// Update is called once per frame
	void Update () {
        if (_isFadingIn)
        {
            if (fader.gameObject.activeSelf)
            {
                if (_currentFadeTime < 1.0f)
                {
                    fader.color = Color.Lerp(fadeColor, transparentColor, _currentFadeTime);
                    _currentFadeTime += Time.deltaTime / fadeDuration;
                }
                else
                {
                    fader.gameObject.SetActive(false);
                }
            }
        }
        else
        {
            if (fader.gameObject.activeSelf)
            {
                if (_currentFadeTime < 1.0f)
                {
                    fader.color = Color.Lerp(transparentColor, fadeColor, _currentFadeTime);
                    _currentFadeTime += Time.deltaTime / fadeDuration;
                }
            }
        }
	}

    public static void LoadScene(string targetScene) {
        destinationSceneName = targetScene;
        Application.backgroundLoadingPriority = ThreadPriority.High;
        if (destinationSceneName != null) {
            SceneManager.LoadScene(loadingSceneName);
        }
    }

    void LoadingStart()
    {
        _currentFadeTime = 0f;
        fader.gameObject.SetActive(true);
    }

    void LoadingEnd()
    {
        _currentFadeTime = 0f;
        _isFadingIn = false;
        fader.gameObject.SetActive(true);
    }

    protected IEnumerator LoadAsync()
    {
        LoadingStart();
        yield return new WaitForSeconds(fadeDuration);

        _asyncOperation = SceneManager.LoadSceneAsync(destinationSceneName, LoadSceneMode.Single);
        _asyncOperation.allowSceneActivation = false;

        while (_asyncOperation.progress < 0.9f)
        {
            yield return null;
        }

        LoadingEnd();
        yield return new WaitForSeconds(fadeDuration + loadCompleteDelay);

        _asyncOperation.allowSceneActivation = true;
    }

    IEnumerator AnimateText() {
        nowLoadingText.text = startingLoadingText;

        while (true)
        {
            yield return new WaitForSeconds(0.2f);
            if (nowLoadingText.text != loopingEndLoadingText)
            {
                nowLoadingText.text += ".";
            }
            else
            {
                nowLoadingText.text = startingLoadingText;
            }
        }
    }
}
