﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PGMStatueController : MonoBehaviour {
    public GameObject eyePart;
    public GameObject hammerPart;
    public GameObject wrenchPart;
    public PedestalController pedestal;
    public string raisedPedestalMessage = "Place statue parts here.";
    public string completedStatueMessage = "All statue parts are assembled.";
    public string activatedPedestalMessage = "(E) Turn Pedestal";
	[FMODUnity.EventRef]
	public string metalPlaceSound;

    private InteractiveObjectController _interactiveController;

	// Use this for initialization
	void Start () {
        _interactiveController = GetComponent<InteractiveObjectController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void CheckStatuePartsForActivation() {
        if (eyePart.activeSelf && hammerPart.activeSelf && wrenchPart.activeSelf) {
            pedestal.canRotate = true;
            _interactiveController.objectMessage = completedStatueMessage;
            pedestal.gameObject.GetComponent<InteractiveObjectController>().objectMessage = activatedPedestalMessage;
        }
    }

    public void SetRaisedPedestalMessage() {
        _interactiveController.objectMessage = raisedPedestalMessage;
    }

    public void Interact(ObjectRayController rayController) {
        if (pedestal.pedestalAnimator.GetBool("HasRaised")) {
            if (rayController.PickupObject != null)
            {
                if (rayController.PickupObject.GetComponent<InteractiveObjectController>().pickupType == PickupObjectType.TYPE_STATUE_EYE)
                {
                    eyePart.SetActive(true);
					FMODUnity.RuntimeManager.PlayOneShot (metalPlaceSound, transform.position);
                    GameObject discardedObj = rayController.PickupObject;
                    rayController.PickupObject = null;
                    Destroy(discardedObj);
                    CheckStatuePartsForActivation();
                }
                else if (rayController.PickupObject.GetComponent<InteractiveObjectController>().pickupType == PickupObjectType.TYPE_STATUE_HAMMER)
                {
                    hammerPart.SetActive(true);
					FMODUnity.RuntimeManager.PlayOneShot (metalPlaceSound, transform.position);
                    GameObject discardedObj = rayController.PickupObject;
                    rayController.PickupObject = null;
                    Destroy(discardedObj);
                    CheckStatuePartsForActivation();
                }
                else if (rayController.PickupObject.GetComponent<InteractiveObjectController>().pickupType == PickupObjectType.TYPE_STATUE_WRENCH)
                {
                    wrenchPart.SetActive(true);
					FMODUnity.RuntimeManager.PlayOneShot (metalPlaceSound, transform.position);
                    GameObject discardedObj = rayController.PickupObject;
                    rayController.PickupObject = null;
                    Destroy(discardedObj);
                    CheckStatuePartsForActivation();
                }
            }
        }
    }
}
