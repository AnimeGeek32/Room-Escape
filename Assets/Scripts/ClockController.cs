﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockController : MonoBehaviour {
    public float startingTimeInSeconds = 300;
    public string timeMessageSuffix = "until the end!";
    [FMODUnity.EventRef] public string tickSoundEvent;
    [FMODUnity.EventRef] public string deathSoundEvent;
    public GameManager gameManager;

    private InteractiveObjectController _interactiveController;
    private float _currentTime;
    private bool _hasInitiatedLoseTransition = false;
    private FMOD.Studio.EventInstance _eventInstance;

	// Use this for initialization
	void Start () {
        _interactiveController = GetComponent<InteractiveObjectController>();
        _currentTime = startingTimeInSeconds;
        int minutes = ((int)(_currentTime / 60f));
        int seconds = ((int)_currentTime) - (minutes * 60);
        _interactiveController.objectMessage = minutes + ":" + seconds.ToString("D2") + " " + timeMessageSuffix;

        if (tickSoundEvent != "") {
            _eventInstance = FMODUnity.RuntimeManager.CreateInstance(tickSoundEvent);
            _eventInstance.start();
        }
	}
	
	// Update is called once per frame
	void Update () {
        _eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform));
	}

    void OnDestroy() {
        _eventInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        _eventInstance.release();
    }

    void FixedUpdate() {
        if (_currentTime <= 0.0f) {
            if (!_hasInitiatedLoseTransition) {
                _eventInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                _eventInstance = FMODUnity.RuntimeManager.CreateInstance(deathSoundEvent);
                _eventInstance.start();
                _hasInitiatedLoseTransition = true;
                gameManager.GoToLoseScene();
            }
        } else {
            _currentTime -= Time.fixedDeltaTime;
            int minutes = ((int)(_currentTime / 60f));
            int seconds = ((int)_currentTime) - (minutes * 60);
            _interactiveController.objectMessage = minutes + ":" + seconds.ToString("D2") + " " + timeMessageSuffix;
        }
    }
}
