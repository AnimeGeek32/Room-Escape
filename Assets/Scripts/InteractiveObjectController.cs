﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PickupObjectType
{
    TYPE_NONE,
    TYPE_STATUE_EYE,
    TYPE_STATUE_HAMMER,
    TYPE_STATUE_WRENCH,
    TYPE_BOOK,
    TYPE_CANDLE
}

public class InteractiveObjectController : MonoBehaviour {
    public string objectMessage;
    public bool canBeInteracted = false;
    public bool isPickupObject = false;
    public PickupObjectType pickupType = PickupObjectType.TYPE_NONE;
    [FMODUnity.EventRef] public string pickupDropSoundEvent;
    public float dropSoundVelocityMagnitudeThreshold = 0.1f;

    private Rigidbody _rigidbody;

	// Use this for initialization
	void Start () {
        _rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (isPickupObject) {
            if (pickupDropSoundEvent != "") {
                Debug.Log("Gameobject: " + gameObject.name + " Drop velocity magnitude: " + _rigidbody.velocity.magnitude);
                if (Mathf.Abs(_rigidbody.velocity.magnitude) >= dropSoundVelocityMagnitudeThreshold) {
                    FMODUnity.RuntimeManager.PlayOneShot(pickupDropSoundEvent, transform.position);
                }
            }
        }
    }
}
