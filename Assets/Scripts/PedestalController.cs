﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class PedestalController : MonoBehaviour {
    public Animator outerRoomAnimator;
    public Animator pedestalAnimator;
    public PGMStatueController statueController;
    [FMODUnity.EventRef] public string raiseAndRotateSoundEvent;
    [FMODUnity.EventRef] public string outerRoomRotateSoundEvent;
    public float raiseDurationInSecs = 2.0f;
    public float rotationDurationInSecs = 5.0f;
    public bool canRotate = false;

    private int numOfCandles = 0;
    private const int kMaxNumOfCandles = 4;
    private FMOD.Studio.EventInstance _eventInstance;
    private FMOD.Studio.EventInstance _outerRoomEventInstance;

	// Use this for initialization
	void Start () {
        if (raiseAndRotateSoundEvent != "") {
            _eventInstance = FMODUnity.RuntimeManager.CreateInstance(raiseAndRotateSoundEvent);
        }
        if (outerRoomRotateSoundEvent != "") {
            _outerRoomEventInstance = FMODUnity.RuntimeManager.CreateInstance(outerRoomRotateSoundEvent);
        }
	}
	
	// Update is called once per frame
	void Update () {
        _eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        _outerRoomEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(outerRoomAnimator.gameObject));
	}

    void OnDestroy()
    {
        _eventInstance.release();
        _outerRoomEventInstance.release();
    }

    public void AddCandle() {
        numOfCandles++;
        if (numOfCandles >= kMaxNumOfCandles) {
            if (!pedestalAnimator.GetBool("HasRaised")) {
                if (raiseAndRotateSoundEvent != "") {
                    StartCoroutine(PlaySoundForDuration(raiseDurationInSecs, false));
                }
                pedestalAnimator.SetBool("HasRaised", true);
                statueController.SetRaisedPedestalMessage();
            }
        }
    }

    public void Interact(ObjectRayController rayController)
    {
        if (canRotate)
        {
            if (pedestalAnimator != null && outerRoomAnimator != null)
            {
                if (!pedestalAnimator.GetBool("HasRotated"))
                {
                    if (raiseAndRotateSoundEvent != "") {
                        StartCoroutine(PlaySoundForDuration(rotationDurationInSecs, true));
                    }
                    pedestalAnimator.SetBool("HasRotated", true);
                    outerRoomAnimator.SetBool("HasRotated", true);
                }
            }
        }
    }

    IEnumerator PlaySoundForDuration(float duration, bool includeOuterRoom) {
        _eventInstance.start();
        if (includeOuterRoom) {
            _outerRoomEventInstance.start();
        }
        yield return new WaitForSeconds(duration);
		_eventInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        if (includeOuterRoom)
        {
            _outerRoomEventInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }
    }
}
