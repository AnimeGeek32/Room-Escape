﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireplaceController : MonoBehaviour {
    public Animator fireplaceDoorsAnimator;
    public Animator fullPedestalAnimator;
    public Animator floorOpeningAnimator;
    public GameObject fire;
    public string activatedFireMessage = "The fireplace is lit.";
    public float floorOpeningDestroyTime = 7.083f;

    private InteractiveObjectController _interactiveController;

	// Use this for initialization
	void Start () {
        _interactiveController = GetComponent<InteractiveObjectController>();

        if (fire.activeSelf) {
            fire.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void ActivateFire() {
        if (!fireplaceDoorsAnimator.GetBool("isOpened")) {
            fire.SetActive(true);
            _interactiveController.objectMessage = activatedFireMessage;
            fireplaceDoorsAnimator.SetBool("isOpened", true);
            StartCoroutine(RaiseFullPedestalProgress());
        }
    }

    public void Interact(ObjectRayController rayController) {
        if (rayController.PickupObject != null) {
            if (rayController.PickupObject.GetComponent<InteractiveObjectController>().pickupType == PickupObjectType.TYPE_BOOK)
            {
                ActivateFire();
                GameObject discardedObj = rayController.PickupObject;
                rayController.PickupObject = null;
                Destroy(discardedObj);
            }
        }
    }

    IEnumerator RaiseFullPedestalProgress() {
        floorOpeningAnimator.SetBool("IsOpen", true);
        Destroy(floorOpeningAnimator.gameObject, floorOpeningDestroyTime);
        floorOpeningAnimator = null;
        yield return new WaitForSeconds(floorOpeningDestroyTime);
        fullPedestalAnimator.SetBool("HasRaised", true);
    }
}
