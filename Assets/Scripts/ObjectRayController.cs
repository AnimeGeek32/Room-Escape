﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRayController : MonoBehaviour {
    public Camera fpsCamera;
    public Transform pickupHolder;
    public LayerMask interactiveLayer;
    public float maxInteractionDistance = 3f;

    private GameObject _interactiveObject = null;
    private GameObject _pickupObject = null;
    private bool _interactButtonPressed = false;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Interact"))
        {
            if (!_interactButtonPressed)
            {
                _interactButtonPressed = true;
                if (_interactiveObject != null)
                {
                    if (_interactiveObject.GetComponent<InteractiveObjectController>().isPickupObject)
                    {
                        if (_pickupObject == null)
                        {
							_interactiveObject.transform.position = pickupHolder.position;
							_interactiveObject.transform.parent = pickupHolder;
							_interactiveObject.transform.rotation = Quaternion.identity;
							_interactiveObject.GetComponent<Rigidbody>().useGravity = false;
							_interactiveObject.GetComponent<Rigidbody>().isKinematic = true;
							_pickupObject = _interactiveObject;
                        }
                    } else {
                        // Added this condition to avoid SendMessage errors
                        if (_interactiveObject.GetComponent<InteractiveObjectController>().canBeInteracted)
                            _interactiveObject.SendMessage("Interact", this);
                    }
                } else {
                    if (_pickupObject != null)
                    {
                        GameObject dropoffObject = _pickupObject;
                        _pickupObject = null;
                        dropoffObject.transform.parent = null;
                        dropoffObject.GetComponent<Rigidbody>().isKinematic = false;
                        dropoffObject.GetComponent<Rigidbody>().useGravity = true;
                    }
                }
            }
        }
        else if (Input.GetButtonUp("Interact"))
        {
            _interactButtonPressed = false;
        }
	}

    private void FixedUpdate()
    {
        RaycastHit hit;
        if (Physics.Raycast(fpsCamera.transform.position, fpsCamera.transform.forward, out hit, maxInteractionDistance, interactiveLayer.value))
        {
            InteractiveObjectController interactiveObjectController = hit.collider.GetComponent<InteractiveObjectController>();
            if (interactiveObjectController)
            {
                HUDManager.ObjectText.text = interactiveObjectController.objectMessage;
                _interactiveObject = hit.collider.gameObject;
            }
        }
        else
        {
            HUDManager.ObjectText.text = "";
            _interactiveObject = null;
        }
    }

    public GameObject PickupObject
    {
        get
        {
            return _pickupObject;
        }
        set
        {
            _pickupObject = value;
        }
    }

    private void OnDrawGizmos()
    {
        Color cameraColor = Color.yellow;
        Debug.DrawRay(fpsCamera.transform.position, fpsCamera.transform.forward * maxInteractionDistance, cameraColor);
    }
}
