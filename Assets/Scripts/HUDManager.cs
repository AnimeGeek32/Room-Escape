﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour {
    public Text objectText;
    public RawImage fader;
    public float fadeDuration = 1.0f;
    public Color fadeColor = new Color(0f, 0f, 0f, 1f);
    public Color transparentColor = new Color(0f, 0f, 0f, 0f);

    private static HUDManager _instance = null;
    private float _currentFadeTime = 0f;

    void Awake()
    {
        if (_instance != this)
        {
            _instance = this;
        }
    }

	// Use this for initialization
	void Start () {
        if (objectText != null)
        {
            objectText.text = "";
        }

        StartFadeIn();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static Text ObjectText
    {
        get
        {
            return _instance.objectText;
        }
    }

    public static void StartFadeIn()
    {
        _instance.StartCoroutine(_instance.FadeIn());
    }

    IEnumerator FadeIn()
    {
        _currentFadeTime = 0f;
        fader.gameObject.SetActive(true);

        while (_currentFadeTime < 1.0f)
        {
            fader.color = Color.Lerp(fadeColor, transparentColor, _currentFadeTime);
            _currentFadeTime += Time.deltaTime / fadeDuration;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        fader.gameObject.SetActive(false);
    }

    public static void StartFadeOut()
    {
        _instance.StartCoroutine(_instance.FadeOut());
    }

    IEnumerator FadeOut()
    {
        _currentFadeTime = 0f;
        fader.gameObject.SetActive(true);

        while (_currentFadeTime < 1.0f)
        {
            fader.color = Color.Lerp(transparentColor, fadeColor, _currentFadeTime);
            _currentFadeTime += Time.deltaTime / fadeDuration;
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    public static bool IsFading()
    {
        return _instance.fader.gameObject.activeSelf;
    }

    private void OnDestroy()
    {
        if (_instance == this)
        {
            _instance = null;
        }
    }
}
