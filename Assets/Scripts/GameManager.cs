﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public string winSceneName;
    public string loseSceneName;
    public float fadeWaitTime = 1.5f;

    private bool isTransitioning = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoToWinScene() {
        if (!isTransitioning) {
            isTransitioning = true;
            StartCoroutine(TransitionProgress(winSceneName));
        }
    }

    public void GoToLoseScene() {
        if (!isTransitioning)
        {
            isTransitioning = true;
            StartCoroutine(TransitionProgress(loseSceneName));
        }
    }

    IEnumerator TransitionProgress(string destinationScene) {
        HUDManager.StartFadeOut();
        yield return new WaitForSeconds(fadeWaitTime);
        SceneManager.LoadScene(destinationScene);
    }
}
