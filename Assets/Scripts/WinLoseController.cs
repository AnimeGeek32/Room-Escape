﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinLoseController : MonoBehaviour {
    public string nextScene;
    public float transitionWaitTime = 1.0f;
    public float timeToReturnToTitleInSecs = 10.0f;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(CountdownToTitle());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            if (!HUDManager.IsFading())
            {
                StartFadeToLoadingScene();
            }
        }
    }

    void StartFadeToLoadingScene()
    {
        StartCoroutine(FadeProgress());
    }

    IEnumerator FadeProgress()
    {
        HUDManager.StartFadeOut();
        yield return new WaitForSeconds(transitionWaitTime);
        SceneManager.LoadScene(nextScene);
    }

    IEnumerator CountdownToTitle() {
        yield return new WaitForSeconds(timeToReturnToTitleInSecs);
        if (!HUDManager.IsFading()) {
            StartFadeToLoadingScene();
        }
    }
}
