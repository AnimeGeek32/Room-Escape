﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameController : MonoBehaviour {
    public string nextScene;
    public float transitionWaitTime = 1.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKey)
        {
            if (!HUDManager.IsFading())
            {
                StartFadeToLoadingScene();
            }
        }
	}

    void StartFadeToLoadingScene()
    {
        StartCoroutine(FadeProgress());
    }

    IEnumerator FadeProgress()
    {
        HUDManager.StartFadeOut();
        yield return new WaitForSeconds(transitionWaitTime);
        LoadingSceneManager.LoadScene(nextScene);
    }
}
