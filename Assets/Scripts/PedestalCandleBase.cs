﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PedestalCandleBase : MonoBehaviour {
    public GameObject candleObject;
    public PedestalController pedestalController;
    public string placedCandleMessage = "The candle is set.";

    private InteractiveObjectController _interactiveController;

	// Use this for initialization
	void Start () {
        _interactiveController = GetComponent<InteractiveObjectController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Interact(ObjectRayController rayController) {
        if (rayController.PickupObject != null) {
            if (rayController.PickupObject.GetComponent<InteractiveObjectController>().pickupType == PickupObjectType.TYPE_CANDLE) {
                // Remove the pickup object only if the candle isn't visible on the base
                if (!candleObject.activeSelf) {
                    candleObject.SetActive(true);
                    GameObject discardedObj = rayController.PickupObject;
                    rayController.PickupObject = null;
                    Destroy(discardedObj);
                    pedestalController.AddCandle();
                    _interactiveController.objectMessage = placedCandleMessage;
                }
            }
        }
    }
}
